﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary; //rsfb help serialization
/// <summary>
/// La clase Controla todos los movimientos del personaje
/// Tambien se encarga de efectuar las animaciones
/// </summary>
public class PlayerCtrl : MonoBehaviour
{
    public float moreVelocity;
    public float forceJump;
    public bool isJumping;
    bool canDoubleJump = false;
    public float delayTimeForJump;

    public bool isGround;
    public Transform feet;
	public LayerMask whatIsGround;

    public float boxwidth;
	public float boxHeight;

    Rigidbody2D rb;
	Animator anim;
	SpriteRenderer sr;
	
	public Transform kunaiIz;
	public Transform kunaiDe;
	public Transform bulletIz;
	public Transform bulletDe;

    public bool leftPressed;
	public bool rigthPressed;


    public int coinValue;
    public GameData data;
    string dataFilePath;
    BinaryFormatter bf;

    void Start () 
	{
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		sr = GetComponent<SpriteRenderer> ();

        bf = new BinaryFormatter();
        dataFilePath = Application.persistentDataPath + "/game.dat";
        Debug.Log(dataFilePath);
    }
    /// <summary>
    /// update es un metodo que viene por defecto en unity donde se ejecuta frame a frame
    /// en internet encontraran mas acerca mas de este metodo y su funcionamiento
    /// </summary>
    void Update()
    {
        isGround = Physics2D.OverlapBox(new Vector2(feet.position.x, feet.position.y), new Vector2(boxwidth, boxHeight), 360f, whatIsGround);
        float playerSpeed = Input.GetAxisRaw("Horizontal");
        playerSpeed = playerSpeed * moreVelocity;

        if (playerSpeed != 0)
            MoveHorizontal(playerSpeed);
        else
            MoveStop();

       
        if (Input.GetButtonDown("Jump"))
            PlayerJummp();
        
        if (Input.GetButtonDown("Fire1"))
            FireKunais();

        if (leftPressed)
        {
            MoveHorizontal(-moreVelocity);
        }
        if (rigthPressed)
        {
            MoveHorizontal(moreVelocity);
        }
    }
    /// <summary>
    /// La funcion en general de este metodo es hacer que el jugador se mueva adelante o atras
    /// recibe un parametro float speed que es la velocidad a la que se movera el jugador
    /// </summary>
    /// <param name="speed"></param>
    void MoveHorizontal(float speed)
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);
        if (speed < 0)
            sr.flipX = true;
        else if (speed > 0)
            sr.flipX = false;

        if (!isJumping)
        {
            anim.SetInteger("State", 1);
        }
    }
    /// <summary>
    /// el jugador se detiene mediante este metodo
    /// si el jugador no se detuviera resbalara por la fuerza aplicada
    /// </summary>
    void MoveStop()
    {
        
        rb.velocity = new Vector2(0, rb.velocity.y);
        if (!isJumping)
        {
            anim.SetInteger("State", 0);
        }
    }
    /// <summary>
    /// ejecuta el salto y doble salto del jugador cuando se presiona space
    /// </summary>
    void PlayerJummp()
    {
        if (isGround)
        {
            isJumping = true;
            rb.AddForce(new Vector2(0, forceJump));
            anim.SetInteger("State", 2);
            Invoke("enableDoubleJump", delayTimeForJump);
        }

        if (canDoubleJump && !isGround)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, forceJump));
            anim.SetInteger("State", 2);
            canDoubleJump = false;
        }
    }
    /// <summary>
    /// habilita el doble salto
    /// </summary>
    void enableDoubleJump()
    {
        canDoubleJump = true;
    }

    /// <summary>
    /// OnDrawGizmos es una funcion de unity que nos ayuda a dibujar formas para referenciar o saber donde esta
    /// este metodo nos sirve para referenciar el isGround con un cuadrado blanco 
    /// </summary>
    void OnDrawGizmos ()
	{
		Gizmos.DrawWireCube (feet.position, new Vector3(boxwidth, boxHeight, 0));
	}


    /// <summary>
    /// bueno el jugador tiene el poder de disparar kunai o balas con este metodo se instancia la bala en la escena y se decide la direccion
    /// </summary>
	void FireKunais ()
	{
        if (sr.flipX)
        {
			Instantiate (bulletIz, kunaiIz.position, Quaternion.identity);
		}
		if (!sr.flipX) {
			Instantiate (bulletDe, kunaiDe.position, Quaternion.identity);
		}
	}
    /// <summary>
    /// con esta funcion se ejecuta una animacion para que el personaje caiga con otra animacion
    /// no aplica aun
    /// </summary>
    void ShowFalling()
    {
        if (rb.velocity.y < 0)
        {
            anim.SetInteger("State", 3);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            GameCtrl.instance.PlayerDiedAnimation(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case "Coin":
                SfxCtrl.instance.ShowCoinSparkle(other.gameObject.transform.position);
                
                break;
            case "Enemy":
                GameCtrl.instance.PlayerDiedAnimation(gameObject);

                break;
            default:
                break;

        }
    }
    /// <summary>
    /// mover izquierda con los controles de mobile
    /// </summary>
    public void MobileMoveLeft()
	{
		leftPressed = true;
	}
    /// <summary>
    /// mover derecha con los controles de mobile
    /// </summary>
	public void MobileMoveRight()
	{
		rigthPressed = true;
	}
    /// <summary>
    /// cuando deje de oprimir una tecla entonces debe detenerse
    /// </summary>
	public void MobileStop()
	{
		leftPressed = false;
		rigthPressed = false;
		MoveStop ();
	}
    /// <summary>
    /// player salto se ejecuta con el boton
    /// </summary>
	public void MobileJump()
	{
		PlayerJummp ();
	}
    /// <summary>
    /// mobile disparo
    /// </summary>
	public void MobileFire()
	{
		FireKunais ();
	}


    public void SaveData ()
    {
        FileStream fs = new FileStream(dataFilePath, FileMode.Create);
        bf.Serialize(fs, data);
        fs.Close();
    }
    public void LoadData ()
    {
        if (File.Exists(dataFilePath))
        {
            FileStream fs = new FileStream(dataFilePath, FileMode.Open);
            data = (GameData) bf.Deserialize(fs);
            //Debug.Log ("numbre coins: " + data.coinCount);
            //txtCoinCount.text = " x " + data.coinCount;
            //txtScore.text = "Score: " + data.score;

            fs.Close();
        }
    }
    void OnEnable ()
    {
        Debug.Log("Data loaded");
        LoadData();
    }
    void OnDisable ()
    {
        Debug.Log("Data saved");
        SaveData();
    }
    void ResetData ()
    {
        FileStream fs = new FileStream(dataFilePath, FileMode.Create);
        data.coinCount = 0;
        data.score = 0;
        //txtCoinCount.text = " x 0";
        //txtScore.text = "Score: " + data.score;
        bf.Serialize(fs, data);
        fs.Close();
    }
}
