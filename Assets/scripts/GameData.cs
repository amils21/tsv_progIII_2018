﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// Game data data model for game
/// </summary>
[Serializable]
public class GameData
{
    public int coinCount;
    public int score;
}
