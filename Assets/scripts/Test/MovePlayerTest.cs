﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerTest : MonoBehaviour
{
    public float playerSpeed;

    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer sr;

    Vector2 pos;
    void Start ()
    {
        rb =  GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();

        // Almacenamos rigidvody (x, y) de la velocidad
        pos = rb.velocity;
        // La velocidad inicial la asignamos a x
        pos.x = playerSpeed;
    }
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            // sr sprite renderer para girar la imagen
            sr.flipX = true;
            // asignamos a pos.x la velocidad en negativo para que vaya al lado deseado(izquierda)
            pos.x = -playerSpeed;
            //ver por consola
            Debug.Log(pos.x);
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            // sr sprite renderer para girar la imagen
            sr.flipX = false;
            // asignamos a pos.x la velocidad en negativo para que vaya al lado deseado(izquierda)
            pos.x = playerSpeed;
            //ver por consola
            Debug.Log(pos.x);
        }
        //si es velocidad constante animacion por defecto
        anim.SetInteger("State", 1);
        // aplicamos velocidad constante, se genera el movimiento aqui
        rb.velocity = pos;
    }
}
