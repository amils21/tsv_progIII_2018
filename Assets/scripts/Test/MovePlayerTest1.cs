﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerTest1 : MonoBehaviour
{
    public float playerSpeed;

    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer sr;

    Vector2 pos;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        // reconoce ingreso datos -1 hasta 1
        float h = Input.GetAxis("Horizontal");
        //Debug.Log(h);

        // dependiendo valor h
        if (h > 0)
        {
            //animacion correr
            anim.SetInteger("State", 1);
            //giro imagen
            sr.flipX = false;
            //genera movimiento
            rb.AddForce(Vector2.left * -playerSpeed * h);
        }
        else if (h < 0)
        {
            anim.SetInteger("State", 1);
            sr.flipX = true;
            rb.AddForce(Vector2.right * playerSpeed * h);
        }
        else if (h == 0)
        {
            // si h es 0  es que dejo de oprimir el teclado

            //animacion idle
            anim.SetInteger("State", 0);
            //detenemos jugador para que no resbale
            rb.velocity = Vector2.zero;
        }
    }
}
