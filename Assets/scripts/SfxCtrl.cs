﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxCtrl : MonoBehaviour
{
    public SFX sfx;
    public static SfxCtrl instance;
    void Awake()
    {
        if (instance == null)
            instance = this;
    }
    /// <summary>
    /// muestra efecto cuando recoge una moneda
    /// </summary>
    /// <param name="pos"></param>
    public void ShowCoinSparkle(Vector3 pos)
    {
        Instantiate(sfx.SFX_Coin_Pickup_Sparkle, pos, Quaternion.identity);
    }
    /// <summary>
    /// muestra efecto cuando el enemigo explota
    /// </summary>
    /// <param name="pos"></param>
    public void EnemyExplotion(Vector3 pos)
    {
        Instantiate(sfx.SFX_Explosion, pos, Quaternion.identity);
    }
    /// <summary>
    /// muestra efecto cuando el enemigo explota
    /// </summary>
    /// <param name="pos"></param>
    public void ShowEnemyPoof(Vector3 pos)
    {
        Instantiate(sfx.SFX_Explosion, pos, Quaternion.identity);
    }
    /// <summary>
    /// cuando rompe el crater instancia los cubos
    /// </summary>
    public void HandleBoxBreaking(Vector3 pos)
    {
        Vector3 pos1 = pos;
        pos1.x -= 0.3f;

        Vector3 pos2 = pos;
        pos2.x += 0.3f;

        Vector3 pos3 = pos;
        pos3.x -= 0.3f;
        pos3.y -= 0.3f;

        Vector3 pos4 = pos;
        pos4.x += 0.3f;
        pos4.y += 0.3f;

        Debug.Log("hola");
        Instantiate(sfx.sfx_fragment_1, pos1, Quaternion.identity);
        Instantiate(sfx.sfx_fragment_2, pos2, Quaternion.identity);
        Instantiate(sfx.sfx_fragment_2, pos3, Quaternion.identity);
        Instantiate(sfx.sfx_fragment_1, pos4, Quaternion.identity);
    }
}
