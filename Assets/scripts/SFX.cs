﻿using UnityEngine;
using System;

[Serializable]
public class SFX
{
    public GameObject sfx_fragment_1; //when pick a box effect
    public GameObject sfx_fragment_2; //when pick a box effect
    public GameObject SFX_Coin_Pickup_Sparkle;
    public GameObject SFX_Explosion;
}
