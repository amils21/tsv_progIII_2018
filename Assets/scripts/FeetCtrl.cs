﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Esta clase esta ligada al playerCtrl
/// ayuda al personaje a detectar el suelo o plataforma
/// repercute en la animacion
/// en las plataformas ayuda a que no resbale
/// </summary>
public class FeetCtrl : MonoBehaviour
{
    //es una variable que pide un gameobject en este caso el mismo personaje
    public GameObject player;
    // necesitamos declarar una variable PlayerCtrl que es el nombre de la clase
    // para trabajar con alguna de sus varibles especificament con el isJumping que se encuentra en PlayerCtrl
    PlayerCtrl playerCtrl;
    /// <summary>
    /// iniciando los valores obtenemos el primer componente
    /// </summary>
    void Start()
    {
        // obtenemos el componente del padre que el el PlayerCtrl 
        //no olvidemos que estamos en feet que es hijo de ninja o vaquero se hubica en los pies
        playerCtrl = gameObject.transform.parent.gameObject.GetComponent<PlayerCtrl>();
    }
    /// <summary>
    /// OnTriggerEnter2D es una funcion de unity por defencto
    /// nos ayuda a detectar todo lo que choca con un trigger
    /// en este caso localizamos un trigger en los pies
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerEnter2D(Collider2D other)
    {
        // en caso de que choque con un objeto que tenga como tag "Ground" sera verdad y ejecutara la funcion
        if (other.gameObject.CompareTag("Ground"))
        {
            // en este caso cuando saltamos isJumping es igual a true
            // cuando cae al suelo nuevamente esta variable tiene que volver a false para que se ejecute la funcion bien idle o correr
            // desde aqui se pone false ya que en el script PlayerCtrl no hay cuando vuelva esta variable a false
            // trabajan conjuntamente los 2 scripts
            playerCtrl.isJumping = false;
        }
        // en caso de que choque con un objeto que tenga como tag "Platform" sera verdad y ejecutara la funcion
        if (other.gameObject.CompareTag("Platform"))
        {
            //igualmente si es una plataforma la tiene que reconocer como suelo para que se efectue la animacion de caminar
            playerCtrl.isJumping = false;
            // para que el personaje se quede en la plataforma y no resbale 
            // el personaje(nija o vaquero) se tiene que volver hijo de la plataforma
            // parent = padre, entonces el player su padre sera y asignamos la transformada de la plataforma
            player.transform.parent = other.gameObject.transform;
        }
    }
    /// <summary>
    /// OnTriggerExit2D es una funcion de unity por defencto
    /// nos ayuda a detectar cuando un personaje salio del trigger
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit2D(Collider2D other)
    {
        // en este caso si sale de el tag plarform sera verdad
        if (other.gameObject.CompareTag("Platform"))
        {
            // y el padre de jugador sera null que ya no tendra padre
            player.transform.parent = null;
        }
    }
}
