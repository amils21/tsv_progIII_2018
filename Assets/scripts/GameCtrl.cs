﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameCtrl : MonoBehaviour
{
    public static GameCtrl instance;
    public float restartDelay;

    public Text txtCoinCount;
    public Text txtScoreCount;

    int countCoin;
    void Awake()
    {
        if (instance == null)
            instance = this;
    }
    void Start()
    {
        countCoin = 0;
    }

    void Update()
    {

    }
    void RestartLevel()
    {
        SceneManager.LoadScene("001_Primer_nivel");
    }
    /// <summary>
    /// animacion de muerte del jugador
    /// </summary>
    /// <param name="player"></param>
    public void PlayerDiedAnimation(GameObject player)
    {
        // botar hacia atras al jugador
        Rigidbody2D rb = player.GetComponent<Rigidbody2D>();
        rb.AddForce(new Vector2(-150, 400));

        // rotar un poco el jugador en el aire
        player.transform.Rotate(new Vector3(0, 0, 45f));

        // desactivar los comandos de movimiento del jugador
        player.GetComponent<PlayerCtrl>().enabled = false;

        //desactivar los colliders en general del jugador
        foreach (Collider2D c2d in player.transform.GetComponents<Collider2D>())
        {
            c2d.enabled = false;
        }

        //desactivar componenetes hijos del jugador
        foreach (Transform child in player.transform)
        {
            child.gameObject.SetActive(false);
        }

        // la camara por ende sigue al jugador entonces tenemos que desactivar el script de la camara para que deje de seguirlo
        Camera.main.GetComponent<CameraCtrl>().enabled = false;

        // velocidad del jugador enviamos a 0 evitar se mueva o active animacion
        rb.velocity = Vector2.zero;

        StartCoroutine("pauseBeforeReload", player);

    }

    IEnumerator pauseBeforeReload(GameObject player)
    {
        yield return new WaitForSeconds(3.0f);
        PlayerDied(player);
    }
    /// <summary>
    /// Player muere
    /// </summary>
    /// <param name="player"></param>
    public void PlayerDied(GameObject player)
    {
        player.SetActive(false);
        Invoke("RestartLevel", restartDelay);
    }
    /// <summary>
    /// si player cae en el agua y otro elemento
    /// </summary>
    /// <param name="player"></param>
    public void PlayerDrowned(GameObject player)
    {
        Invoke("RestartLevel", restartDelay);
    }

    public void updateCoinCount()
    {
        countCoin++;
        txtCoinCount.text = "X " + countCoin;
        txtScoreCount.text = "Score: " + countCoin;
    }
    /// <summary>
    /// El metodo se ejecuta cuando el kunai o disparo choca con el enemigo
    /// </summary>
    /// <param name="other"></param>
    public void BulletHitEnemy(Transform enemy)
    {
        // muestra la explosion al impactar
        Vector3 pos = enemy.position;
        pos.z = 20f;
        SfxCtrl.instance.EnemyExplotion(pos);

        //Destruye el enemigo
        Destroy(enemy.gameObject);

        //actualiza el score
        updateCoinCount();

    }

    public void PlayerStompsEnemy(GameObject enemy)
    {
        //Cambiar Tag del enemigo
        enemy.tag = "Untagged";
        // destruir enemigo
        Destroy(enemy);
        //actualizar score
        updateCoinCount();
    }
}
