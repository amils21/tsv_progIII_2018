﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 
/// </summary>
public class FlyActivator : MonoBehaviour {
    public GameObject bee;

    FlyEnemyAI feai;
    void Start ()
    {
        feai = bee.GetComponent<FlyEnemyAI>();
	}
	
	void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            feai.ActivatedFlyMoster(other.gameObject.transform.position);

        }
	}
}
