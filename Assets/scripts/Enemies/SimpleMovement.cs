﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour {
    public float speed;
    Rigidbody2D rb;
    SpriteRenderer sr;
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
	}
	
	void Update ()
    {
        move();
        setStartDirection();
    }

    void move()
    {
        Vector2 temp = rb.velocity;
        temp.x = speed;
        rb.velocity = temp;
    }
    void setStartDirection()
    {
        if (speed > 0)
        {
            sr.flipX = true;
        } else {
            sr.flipX = false;
        }
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        /*
        if (!other.gameObject.CompareTag("Player"))
        {
            flipOnCollision();
        }
        */
        if (other.gameObject.CompareTag("BlockEnemy"))
        {
            flipOnCollision();
        }
    }
    void flipOnCollision()
    {
        speed = -speed;
        setStartDirection();
    }
}
