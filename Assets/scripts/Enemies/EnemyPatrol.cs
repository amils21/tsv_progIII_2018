﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    public Transform LeftBound, rightBound;
    public float speed;

    public float maxDelay, minDelay;
    bool canTurn;
    float originalSpeed;

    Rigidbody2D rb;
    SpriteRenderer sr;
    Animator anim;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        SetStartingDirection();

        canTurn = true;
    }

    void Update()
    {
        move();
        FlipOnEdges();
    }

    void move()
    {
        Vector2 temp = rb.velocity;
        temp.x = speed;
        rb.velocity = temp;
    }

    void SetStartingDirection()
    {
        if (speed > 0)
        {
            sr.flipX = true;
        }
        else if (speed < 0)
        {
            sr.flipX = false;
        }
    }
    void FlipOnEdges()
    {
        if (sr.flipX && transform.position.x >= rightBound.position.x)
        {
            /*
            sr.flipX = false;
            speed = -speed;
            */
            if (canTurn)
            {
                canTurn = false;
                originalSpeed = speed;
                speed = 0;
                StartCoroutine("TurnLeft", originalSpeed);
            }
        } else if (!sr.flipX && transform.position.x <= LeftBound.position.x) {
            /*sr.flipX = true;
            speed = -speed;*/
            if (canTurn)
            {
                canTurn = false;
                originalSpeed = speed;
                speed = 0;
                StartCoroutine("TurnRight", originalSpeed);
            }
        }
    }

    IEnumerator TurnLeft(float originalSpeed)
    {
        //isIdle
        anim.SetBool("isIdle", true);
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        anim.SetBool("isIdle", false);
        sr.flipX = false;
        speed = -originalSpeed;
        canTurn = true;
    }
    IEnumerator TurnRight(float originalSpeed)
    {
        anim.SetBool("isIdle", true);
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        anim.SetBool("isIdle", false);
        sr.flipX = true;
        speed = -originalSpeed;
        canTurn = true;
    }
    void OnDrawGizmos()
    {
        Gizmos.DrawLine(LeftBound.position, rightBound.position);
    }
}
