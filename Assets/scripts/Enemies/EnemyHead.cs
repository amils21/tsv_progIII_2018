﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHead : MonoBehaviour {
    public GameObject enemy;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("PlayerFeet"))
        {
            GameCtrl.instance.PlayerStompsEnemy(enemy);
            SfxCtrl.instance.ShowEnemyPoof(enemy.transform.position);  
        }
    }
}
