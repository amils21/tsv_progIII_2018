﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//doTween
using DG.Tweening;
/// <summary>
/// 
/// </summary>
public class FlyEnemyAI : MonoBehaviour
{
    public float delayEnemyDestroy;
    public float beeSpeed;

	void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.CompareTag("Ground") || other.gameObject.CompareTag("Player"))
        {
            SfxCtrl.instance.EnemyExplotion(other.gameObject.transform.position);
            Destroy(gameObject, delayEnemyDestroy);
        }
	}

    public void ActivatedFlyMoster(Vector3 playerPos)
    {
        transform.DOMove(playerPos, beeSpeed, false);
    }
}
